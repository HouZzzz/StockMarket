package StockMarket.Stocks;

import StockMarket.Main;

import java.util.Random;
import java.util.Scanner;

public class Stock {
    Random random = new Random();
    Scanner scanner = new Scanner(System.in);

    // напрпавление обновления цены
    private boolean up = random.nextBoolean();

    // сколько раз акция поменяла цену в одну сторону
    private byte didntChange = 0;

    // количество месяцев до изменения направления цены
    private int bound = 10;
    private int monthsToChange = 12 - CustomRandom(4,bound);

    // сколько игрок потратил денег на эту акцию
    private int boughtFor = 0;

    // цена акции
    private long price;

    // изначальная цена акции
    private int startPrice;

    // сколько игрок имеет этих акций
    private int userGot = 0;

    public void UpdateStockPrice() {
        int updatePower = random.nextInt(2);

        int change = ((startPrice / 93) * (11 - random.nextInt(10)));

        if (up) {
            switch (updatePower) {
                case 0:
                    price += change;
                    break;
                case 1:
                    price += (change * 2);
                    break;
                default:
                    break;
            }
        } else {
            switch (updatePower) {
                case 0:
                    if (price - change > 0) {
                        price -= change;
                    } else {
                        price += change;
                    }
                    break;
                case 1:
                    if (price - (change * 2) > 0) {
                        price -= (change * 2);
                    } else {
                        price += (change * 2);
                    }
                    break;
                default:
                    break;
            }
        }

        didntChange++;
        if (didntChange == monthsToChange) {
            didntChange = 0;
            monthsToChange = 12 - CustomRandom(4, bound);
            up = !up;
        }
    }

    public void GetInfo() {
        System.out.println("[~] Цена: " + price);
        System.out.println("[~] У вас в наличии: " + userGot);
        if (userGot > 0) {
            System.out.println("[~] Вы потратили на эту акцию " + boughtFor
                    + " рублей. Вы окупитесь если акция стоит больше чем " + (boughtFor / userGot) + " рублей");
        }

        System.out.println("\n[~] Купить акцию");
        System.out.println("[~] Продать акцию");
        System.out.println("[~] Выход\n");

        System.out.print("Ваш выбор: ");
        int selected = scanner.nextInt();

        switch (selected) {
            case 1:
                BuyStock();
                break;
            case 2:
                SellStock();
                break;
            case 3:
                StocksMenu.SoutStocks();
                break;
            default:
                break;
        }

    }

    public void BuyStock() {
        System.out.println("[~] Сколько акций вы хотите купить?");
        System.out.println("[~] У вас " + Main.money + " рублей, максимум вы можете купить " + (Main.money / price)
                + " акций компании.");
        int buy = scanner.nextInt();

        if ((buy * price) > Main.money) {
            System.out.println("[~] Не хватает баланса");

        } else {
            userGot += buy;
            Main.money -= price * buy;
            boughtFor += price * buy;
        }
    }

    public void SellStock() {
        System.out.println("[~] Сколько акций вы хотите продать?");
        System.out.println("[~] Если вы продадите все акции то получите " + (userGot * price) + " рублей");
        int sell = scanner.nextInt();

        while (sell > userGot) {
            System.out.println("У вас нету столько акций");
            sell = scanner.nextInt();
        }

        userGot -= sell;
        Main.money += price * sell;
        if (userGot == 0) {
            boughtFor = 0;
        }
    }

    public long ifSellAll() {
        return userGot * price;
    }


    public int getBoughtFor() {
        return boughtFor;
    }

    public long getPrice() {
        return price;
    }

    public int getUserGot() {
        return userGot;
    }

    int CustomRandom(int from, int to) {
        Random boundRand = new Random();
        return from + boundRand.nextInt(to - from);
    }

    public Stock(int price) {
        this.price = price;
        startPrice = price;
    }
}
