package StockMarket.Selections;

import StockMarket.Main;
import StockMarket.Stocks.StockSets;

public class NextMonth {
    public static void SkipMonth() throws InterruptedException {

        // увеличение возраста
        if (Main.months < 11) {
            Main.months += 1;
        } else {
            Main.months -= 11;
            Main.years += 1;
        }

        // плата за кредит
        if (Bank.Credit) {
            Main.money -= Bank.monthlyFee;
            Bank.CreditMoney -= Bank.monthlyFee;

            if (Bank.CreditMoney < 1) {
                System.out.println("\n[*] Банк: Поздравляем, вы погасили кредит!");
                Bank.PaidCredits += 1;
                if (Bank.PaidCredits >= 3 && Bank.PaidCredits < 6) {
                    System.out.println("[*] Ваш кредитный рейттинг 1/3!");
                    Bank.CreditRating = 1;
                    Bank.MaxCredit = 500000;
                } else if (Bank.PaidCredits >= 6 && Bank.PaidCredits < 10) {
                    System.out.println("[*] Ваш кредитный рейтинг 2/3!");
                    Bank.CreditRating = 2;
                    Bank.MaxCredit = 1500000;
                } else if (Bank.PaidCredits >= 10) {
                    System.out.println("[*] Ваш кредитный рейтинг 3/3!");
                    Bank.CreditRating = 3;
                    Bank.MaxCredit = 100000000;
                }
                Bank.Credit = false;
                Thread.sleep(2000);

            }
        }
        // начисление зарплаты
        if (Work.haveWork) {

            Work.workingMonths += 1;

            if (Work.workingMonths <= Work.increase1) {
                Work.PostStatus = "Секретарь";
                Main.money += Work.salary1;
            } else if (Work.workingMonths > Work.increase1 && Work.workingMonths <= Work.increase2) {
                Work.PostStatus = "Кассир";
                Main.money += Work.salary2;
            } else if (Work.workingMonths > Work.increase2 && Work.workingMonths <= Work.increase3) {
                Work.PostStatus = "Главный бухгалтер";
                Main.money += Work.salary3;
            } else if (Work.workingMonths > Work.increase3 && Work.workingMonths <= Work.increase4) {
                Work.PostStatus = "Финансовый директор";
                Main.money += Work.salary4;
            } else {
                Work.PostStatus = "Генеральный директор";
                Main.money += Work.salary5;
            }
        } else {
            Work.monthlyTry = false;
        }

        // изменение цен всех акций
        UpdateStockPrice();

        // оплата ссуды
        if (Bank.Loan) {

            Main.money += Bank.LoanMonthlyFee;
            Bank.LoanMoney -= Bank.LoanMonthlyFee;

            if (Bank.LoanMoney < 1) {
                System.out.println("\n[*] Вам выплатили ссуду!");
                Bank.Loan = false;
                Thread.sleep(2000);
            }
        }
    }

    public static void UpdateStockPrice() {
        StockSets stockSets = new StockSets();

        stockSets.Sberbank.UpdateStockPrice();
        stockSets.Gasprom.UpdateStockPrice();
        stockSets.Lukoil.UpdateStockPrice();
        stockSets.Tinkoff.UpdateStockPrice();
        stockSets.Magnit.UpdateStockPrice();
        stockSets.Yandex.UpdateStockPrice();
        stockSets.Vk.UpdateStockPrice();
        stockSets.Mts.UpdateStockPrice();
        stockSets.Aerofloat.UpdateStockPrice();
        stockSets.Ozon.UpdateStockPrice();
        stockSets.McDonalds.UpdateStockPrice();
        stockSets.Nike.UpdateStockPrice();
        stockSets.Disney.UpdateStockPrice();
        stockSets.Mastercard.UpdateStockPrice();
        stockSets.Ford.UpdateStockPrice();
        stockSets.Microsoft.UpdateStockPrice();
        stockSets.Apple.UpdateStockPrice();
        stockSets.Tesla.UpdateStockPrice();
        stockSets.Starbucks.UpdateStockPrice();
        stockSets.Amazon.UpdateStockPrice();
        stockSets.Intel.UpdateStockPrice();
        stockSets.Netflix.UpdateStockPrice();
        stockSets.PayPal.UpdateStockPrice();
        stockSets.Nvidia.UpdateStockPrice();
        stockSets.Ferrari.UpdateStockPrice();
    }
}