package StockMarket.Selections;

import java.util.Random;

public class Work {

    // имеет ли игрок работу?
    public static boolean haveWork = false;

    // пробовал ли он найти работу в этом месяце?
    public static boolean monthlyTry = false;

    // счетчиков месяцов работы
    public static int workingMonths = 0;

    // шанс устроиться на работу (10% здесь значит шанс 90%)
    public static int FindWorkChance = 10;

    // зарплаты
    public static int salary1 = 10;
    public static int salary2 = 15;
    public static int salary3 = 20;
    public static int salary4 = 30;
    public static int salary5 = 50;

    // месяцы до повышения
    public static int increase1 = 12;
    public static int increase2 = 24;
    public static int increase3 = 48;
    public static int increase4 = 90;

    // профессия
    public static String PostStatus;

    public static void getWork() throws InterruptedException {

        Random random = new Random();

        // попытка устроиться на работу
        if (!haveWork && !monthlyTry) {
            if (random.nextInt(100) > 70) {
                haveWork = true;

                System.out.println("[-] Вы устроилиси на работу!");

            } else {
                System.out.println("[-] Увы, вас не приняли на работу. Удачи в следуйющий раз!");
                monthlyTry = true;
            }
        } else if (monthlyTry) {
            System.out.println("[-] Вы уже пробовали устроиться на работу в этом месяце!");
        } else {
            System.out.println("[-] У вас уже есть работа!");
        }

        Thread.sleep(2000);
    }

}
