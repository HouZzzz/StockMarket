package StockMarket;

import StockMarket.Selections.*;

public class SetDifficulty {

    // методы изменяют значения переменных, которые влияют на игру
    public static void Easy() throws InterruptedException {

        Main.money = 30000;

        Bank.CreditPercent = 20;
        Bank.LoanPercent = 20;
        Bank.LoanTime = 24;

        Work.FindWorkChance = 55;

        Work.salary1 = 10000;
        Work.salary2 = 20000;
        Work.salary3 = 35000;
        Work.salary4 = 50000;
        Work.salary4 = 120000;

        Work.increase1 = 24;
        Work.increase2 = Work.increase1 + 30;
        Work.increase3 = Work.increase2 + 30;
        Work.increase4 = Work.increase3 + 50;

        System.out.println("[~] Удачи");
        Thread.sleep(2000);

    }

    public static void Middle() throws InterruptedException {

        Main.money = 15000;

        Bank.CreditPercent = 30;
        Bank.LoanPercent = 30;
        Bank.LoanTime = 36;

        Work.FindWorkChance = 80;

        Work.salary1 = 7000;
        Work.salary2 = 15000;
        Work.salary3 = 30000;
        Work.salary4 = 42000;
        Work.salary4 = 100000;

        Work.increase1 = 30;
        Work.increase2 = Work.increase1 + 35;
        Work.increase3 = Work.increase2 + 35;
        Work.increase4 = Work.increase3 + 50;

        System.out.println("[~] Удачи");
        Thread.sleep(2000);
    }

    public static void Hard() throws InterruptedException {

        Main.money = 7000;

        Bank.CreditPercent = 35;
        Bank.LoanPercent = 35;
        Bank.LoanTime = 48;

        Work.FindWorkChance = 90;

        Work.salary1 = 5000;
        Work.salary2 = 10000;
        Work.salary3 = 25000;
        Work.salary4 = 40000;
        Work.salary4 = 110000;

        Work.increase1 = 35;
        Work.increase2 = Work.increase1 + 40;
        Work.increase3 = Work.increase2 + 40;
        Work.increase4 = Work.increase3 + 50;

        System.out.println("[~] Да вы хардкорщик");
        Thread.sleep(2000);
    }
}
