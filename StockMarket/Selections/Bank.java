package StockMarket.Selections;

import java.util.Random;
import java.util.Scanner;

import StockMarket.Main;

public class Bank {
    public static Scanner scanner = new Scanner(System.in);
    public static Random random = new Random();

    // информация о кредите
    public static boolean Credit = false;
    public static int CreditTime;
    public static int CreditMoney;
    public static int monthlyFee;
    public static int CreditPercent = 1;

    public static int CreditRating = 0;

    public static int PaidCredits = 0;

    public static int MaxCredit = 60000;

    // информация о ссуде
    public static boolean Loan = false;
    public static int LoanTime;
    public static int LoanMoney;
    public static int LoanMonthlyFee;
    public static int LoanPercent = 1;

    public static void BankMenu() throws InterruptedException {

        System.out.println("\n[*] Взять кредит");
        System.out.println("[*] Оплатить часть кредита");
        System.out.println("[*] Дать ссуду");
        System.out.print("\n[*] Ваш выбор: ");

        // выбор игрока
        int selected = scanner.nextInt();

        SelectCases.RunBank(selected);
    }

    public static void getCredit() throws InterruptedException {

        System.out.println("\n[*] На какую сумму взять кредит? (минимум 5000, максимум " + MaxCredit + ")");
        while (CreditMoney < 5000 || CreditMoney > MaxCredit) {
            CreditMoney = scanner.nextInt();

            if (CreditMoney < 5000) {
                System.out.println("[*] Минимальная сумма кредита - 5000 рублей!");
            } else if (CreditMoney > MaxCredit) {
                System.out.println("[*] Максимальная сумма кредита - " + MaxCredit + " рублей!");
            }
        }

        System.out.println("[*] На какой срок взять кредит? (месяцы)");
        while (CreditTime < 2) {
            CreditTime = scanner.nextInt();

            if (CreditTime < 2) {
                System.out.println("[*] Минимум 2 месяца!");
            }
        }

        System.out.println("[*] Каждый месяц вы будете должны платить "
                + ((CreditMoney + ((CreditMoney / 100) * CreditPercent)) / CreditTime) + " рублей");

        System.out.println("\n[*] Подтвердить? (1 - да / 2 - нет)");

        int verify = scanner.nextInt();

        switch (verify) {
            case 1:
                System.out.println("[*] Вы подтвердили кредит!");
                Main.money += CreditMoney;
                Credit = true;
                monthlyFee = ((CreditMoney + ((CreditMoney / 100) * CreditPercent)) / CreditTime);
                break;
            case 2:
                System.out.println("[*] Вы не подтвердили кредит!");
                Credit = false;
                CreditMoney = 0;
                CreditTime = 0;
                break;
            default:
                break;
        }
        if (verify > 2) {
            System.out.println("[*] Некорректный ввод, вы не подтвердили кредит");
            Credit = false;
            CreditMoney = 0;
            CreditTime = 0;
        }
        CreditMoney = CreditMoney + ((CreditMoney / 100) * CreditPercent);
        Thread.sleep(2000);

    }

    public static void PayCredit() throws InterruptedException {

        if (Credit) {
            System.out.println("[*] Какую сумму от кредита вы хотите оплатить?");
            int instaPay = scanner.nextInt();

            while (instaPay > CreditMoney) {
                System.out.println("[*] Мы ценим вашу щедрость но не стоит переплачивать :)\n[*] Вам осталось оплатить "
                        + CreditMoney);
                System.out.println("[*] Какую сумму от кредита вы хотите оплатить?");
                instaPay = scanner.nextInt();
            }

            CreditMoney -= instaPay;

            System.out.println("[*] Спасибо! Теперь вам осталось заплатить " + CreditMoney);

        } else {
            System.out.println("[*] У вас нету кредита!");
        }
        Thread.sleep(2000);

    }

    public static void GiveLoan() {

        if (Loan) {
            System.out.println("[*] У вас уже есть ссуда");
        } else {
            LoanTime -= random.nextInt(15);

            System.out.println("\n[*] Какую сумму вы хотите дать в долг?");
            LoanMoney = scanner.nextInt();

            System.out.println(
                    "[*] Вам вернут " + (LoanMoney + ((LoanMoney / 100) * LoanPercent)) + " в течении " + LoanTime
                            + " месяцев");
            System.out.println("[*] Каждый месяц вам будут платит "
                    + ((LoanMoney + ((LoanMoney / 100) * LoanPercent)) / LoanTime));

            Loan = true;
            LoanMonthlyFee = ((LoanMoney + ((LoanMoney / 100) * LoanPercent)) / LoanTime);
            Main.money -= LoanMoney;
            LoanMoney = (LoanMoney + ((LoanMoney / 100) * LoanPercent));

        }

    }
}
