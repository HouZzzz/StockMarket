package StockMarket.Stocks;

public class StockSets {
    public static Stock Aerofloat = new Stock(305);
    public static Stock Amazon = new Stock(154736);
    public static Stock Apple = new Stock(50209);
    public static Stock Disney = new Stock(7352);
    public static Stock Ferrari = new Stock(13314);
    public static Stock Ford = new Stock(927);
    public static Stock Gasprom = new Stock(2354);
    public static Stock Intel = new Stock(2980);
    public static Stock Lukoil = new Stock(4623);
    public static Stock Magnit = new Stock(4700);
    public static Stock Mastercard = new Stock(23448);
    public static Stock McDonalds = new Stock(16493);
    public static Stock Microsoft = new Stock(18282);
    public static Stock Mts = new Stock(210);
    public static Stock Netflix = new Stock(12519);
    public static Stock Nike = new Stock(7882);
    public static Stock Nvidia = new Stock(12386);
    public static Stock Ozon = new Stock(945);
    public static Stock PayPal = new Stock(5696);
    public static Stock Sberbank = new Stock(1241);
    public static Stock Starbucks = new Stock(5166);
    public static Stock Tesla = new Stock(58357);
    public static Stock Tinkoff = new Stock(2382);
    public static Stock Vk = new Stock(378);
    public static Stock Yandex = new Stock(1687);
}
