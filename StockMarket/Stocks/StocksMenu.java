package StockMarket.Stocks;

import java.util.Scanner;

import StockMarket.Selections.SelectCases;

public class StocksMenu {

        public static Scanner scanner = new Scanner(System.in);

        public static void SoutStocks() {
                System.out.println("");
                System.out.println("[1] Сбербанк    | [2] Газпром    | [3] Лукоил    | [4] Тинькофф      | [5] Магнит");
                System.out.println("[6] Яндекс      | [7] ВК         | [8] МТС       | [9] Аэрофлот      | [10] ОЗОН");
                System.out.println("[11] McDonalds  | [12] Nike      | [13] Disney   | [14] Mastercard   | [15] Ford");
                System.out.println(
                                "[16] Microsoft  | [17] Apple     | [18] Tesla    | [19] Starbucks    | [20] Amazon");
                System.out.println(
                                "[21] Intel      | [22] Netflix   | [23] PayPal   | [24] Nvidia       | [25] Ferrari\n");
                System.out.println("\n[0] Выход\n");
                System.out.print("[+] Ваш выбор: ");

                int selected = scanner.nextInt();

                SelectCases.RunStocks(selected);
        }

        public static void SoutStocksPrice() {
                StockSets stockSets = new StockSets();

                System.out.println("");
                System.out.println("[1] Сбербанк    | [2] Газпром    | [3] Лукоил    | [4] Тинькофф      | [5] Магнит");
                System.out.println("[6] Яндекс      | [7] ВК         | [8] МТС       | [9] Аэрофлот      | [10] ОЗОН");
                System.out.println("[11] McDonalds  | [12] Nike      | [13] Disney   | [14] Mastercard   | [15] Ford");
                System.out.println(
                                "[16] Microsoft  | [17] Apple     | [18] Tesla    | [19] Starbucks    | [20] Amazon");
                System.out.println(
                                "[21] Intel      | [22] Netflix   | [23] PayPal   | [24] Nvidia       | [25] Ferrari\n");
                System.out.println("");

                System.out.println("[1] " + stockSets.Sberbank.getPrice() + " [2] " + stockSets.Gasprom.getPrice() + " [3] " + stockSets.Lukoil.getPrice()
                                + " [4] "
                                + stockSets.Tinkoff.getPrice() + " [5] " + stockSets.Magnit.getPrice() + "\n");

                System.out.println("[6] " + stockSets.Yandex.getPrice() + " [7] " + stockSets.Vk.getPrice() + " [8] " + stockSets.Mts.getPrice() + " [9] "
                                + stockSets.Aerofloat.getPrice() + " [10] " + stockSets.Ozon.getPrice() + "\n");

                System.out.println("[11] " + stockSets.McDonalds.getPrice() + " [12] " + stockSets.Nike.getPrice() + " [13] " + stockSets.Disney.getPrice()
                                + " [14] " + stockSets.Mastercard.getPrice() + " [15] " + stockSets.Ford.getPrice() + "\n");

                System.out.println("[16] " + stockSets.Microsoft.getPrice() + " [17] " + stockSets.Apple.getPrice() + " [18] " + stockSets.Tesla.getPrice()
                                + " [19] " + stockSets.Starbucks.getPrice() + " [20] " + stockSets.Amazon.getPrice() + "\n");

                System.out.println("[21] " + stockSets.Intel.getPrice() + " [22] " + stockSets.Netflix.getPrice() + " [23] " + stockSets.PayPal.getPrice()
                                + " [24] " + stockSets.Nvidia.getPrice() + " [25] " + stockSets.Ferrari.getPrice() + "\n");

                System.out.println("\n[0] Выход\n");
                System.out.print("[+] Ваш выбор: ");

                int selected = scanner.nextInt();

                SelectCases.RunStocks(selected);

        }
}
