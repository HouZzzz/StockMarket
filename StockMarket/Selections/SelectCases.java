package StockMarket.Selections;

import StockMarket.SetDifficulty;
import StockMarket.Stocks.StockSets;
import StockMarket.Stocks.StocksMenu;

public class SelectCases {
    public void RunFile(int num) throws InterruptedException {


        // Выбор класса в зависимости от выбора игрока
        switch (num) {
            case 1:
                NextMonth.SkipMonth();
                break;
            case 2:
                Bank.BankMenu();
                break;
            case 3:
                Work.getWork();
                break;
            case 4:
                StocksMenu.SoutStocks();
                break;
            case 5:
                StocksMenu.SoutStocksPrice();
                break;
            case 6:
                Profile.ProfileMenu();
                break;
            case 7:
                Calculator.Calculate();
            default:
                break;
        }
    }

    // вызов методов банка из меню
    public static void RunBank(int num) throws InterruptedException {
        switch (num) {
            case 1:
                Bank.getCredit();
                break;
            case 2:
                Bank.PayCredit();
            case 3:
                Bank.GiveLoan();
                break;

            default:
                break;
        }
    }

    // вывод информации о акции
    public static void RunStocks(int num) {
        StockSets stockSets = new StockSets();

        switch (num) {
            case 1:
                stockSets.Sberbank.GetInfo();
                break;
            case 2:
                stockSets.Gasprom.GetInfo();
                break;
            case 3:
                stockSets.Lukoil.GetInfo();
                break;
            case 4:
                stockSets.Tinkoff.GetInfo();
                break;
            case 5:
                stockSets.Magnit.GetInfo();
                break;
            case 6:
                stockSets.Yandex.GetInfo();
                break;
            case 7:
                stockSets.Vk.GetInfo();
                break;
            case 8:
                stockSets.Mts.GetInfo();
                break;
            case 9:
                stockSets.Aerofloat.GetInfo();
                break;
            case 10:
                stockSets.Ozon.GetInfo();
                break;
            case 11:
                stockSets.McDonalds.GetInfo();
                break;
            case 12:
                stockSets.Nike.GetInfo();
                break;
            case 13:
                stockSets.Disney.GetInfo();
                break;
            case 14:
                stockSets.Mastercard.GetInfo();
                break;
            case 15:
                stockSets.Ford.GetInfo();
                break;
            case 16:
                stockSets.Microsoft.GetInfo();
                break;
            case 17:
                stockSets.Apple.GetInfo();
                break;
            case 18:
                stockSets.Tesla.GetInfo();
                break;
            case 19:
                stockSets.Starbucks.GetInfo();
                break;
            case 20:
                stockSets.Amazon.GetInfo();
                break;
            case 21:
                stockSets.Intel.GetInfo();
                break;
            case 22:
                stockSets.Netflix.GetInfo();
                break;
            case 23:
                stockSets.PayPal.GetInfo();
                break;
            case 24:
                stockSets.Nvidia.GetInfo();
                break;
            case 25:
                stockSets.Ferrari.GetInfo();
                break;
            default:
                break;
        }
    }

    // вызов методов калькулятора
    public static void CalculatoDoSelect(String symbol, int a, int b) throws InterruptedException {
        if (symbol.equals("+")) {
            Calculator.Addition(a, b);
        } else if (symbol.equals("-")) {
            Calculator.Subtraction(a, b);
        } else if (symbol.equals("*")) {
            Calculator.Multiply(a, b);
        } else if (symbol.equals("/")) {
            Calculator.Division(a, b);
        }
    }

    // вывов методов кошелька
    public static void RunProfile(int num) throws InterruptedException {
        switch (num) {
            case 1:
                Profile.MyStocks();
                break;
            case 2:
                Profile.MoneyInStocks();
                break;
            case 3:
                Profile.MyStocksPrice();
                break;
            default:
                break;
        }
    }

    public static void SetDifficulty(int num) throws InterruptedException {
        switch (num) {
            case 1:
                SetDifficulty.Easy();
                break;
            case 2:
                SetDifficulty.Middle();
                break;
            case 3:
                SetDifficulty.Hard();
                break;
            default:
                break;
        }
    }

}
