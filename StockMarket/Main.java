package StockMarket;

import java.util.InputMismatchException;
import java.util.Scanner;

import StockMarket.Selections.*;
import StockMarket.Stocks.StockSets;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    // общая статистика
    public static int years = 20;
    public static int months = 0;
    public static long money = 30000L;

    public static void main(String[] args) throws InterruptedException {
        StartGame();
    }

    public static void MainMenu() throws InterruptedException {
        SelectCases selectCases = new SelectCases();

        int selected = 0;

        // интерфейс
        SoutStat();
        System.out.println("-[1] Следующий месяц");
        System.out.println("--[2] Банк");
        System.out.println("---[3] Устроиться на работу");
        System.out.println("----[4] Акции");
        System.out.println("-----[5] Цены акций");
        System.out.println("------[6] Профиль");
        System.out.println("-------[7] Калькулятор");
        System.out.println("--------[8] Потребности\n");
        System.out.print("[+] Ваш выбор: ");

        // Пользователь вводит свой выбор
        try {
            selected = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("[~] Ошибка ввода");
            Thread.sleep(2000);
        }
        selectCases.RunFile(selected);
    }

    public static void SoutStat() {
        System.out.println("\n[P] " + money);

    }

    public static void StartGame() throws InterruptedException {
        System.out.println(
                "\n[~] Привет! \nЦель игры - иметь на счету 1 миллиард рублей до 60 лет.\nРаботай, покупай и продавай акции.\n");
        System.out.println("[~] Выбери уровень сложности: ");
        System.out.println("[1] Легкий");
        System.out.println("[2] Средний");
        System.out.println("[3] Сложный");

        System.out.print("\n[~] Ваш выбор: ");

        int selected = scanner.nextInt();

        SelectCases.SetDifficulty(selected);

        while (true) {

            MainMenu();

            // проверки состояния игры и сообщения о проигрыше или выигрыше
            if (years == 60 && money < 1000000000) {
                System.out.println(
                        "[-] Увы, вы проиграли... Вам уже наступило 60 лет но миллиарда на счету все еще небыло");
                break;
            } else if (years < 60 && money >= 1000000000) {
                System.out.println("[+] Поздрявляю, вы победили! У вас на счету целый миллиард, а вам " + years + "!");
                break;
            } else if (money < -100000) {
                System.out.println("[-] Увы, вы проиграли... Вы ушли в огромный минус");
                break;
            }
        }
    }

}