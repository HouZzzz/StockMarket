package StockMarket.Selections;

import java.util.Scanner;

import StockMarket.Main;

public class Calculator {

    public static Scanner scanner = new Scanner(System.in);

    public static void Calculate() throws InterruptedException {
        System.out.println("[v] У вас " + Main.money + " рублей");
        System.out.print("[v] Что делать? (+ - * /)  ");
        String selection = scanner.next();
        System.out.print("\n[v] a: ");
        int a = scanner.nextInt();
        System.out.print("[v] b:");
        int b = scanner.nextInt();

        SelectCases.CalculatoDoSelect(selection, a, b);
    }

    public static void Addition(int a, int b) throws InterruptedException {
        System.out.println("Ответ: " + (a + b));
        CustomSleep();
    }

    public static void Subtraction(int a, int b) throws InterruptedException {
        System.out.println("Ответ: " + (a - b));
        CustomSleep();
    }

    public static void Multiply(int a, int b) throws InterruptedException {
        System.out.println("Ответ: " + (a * b));
        CustomSleep();
    }

    public static void Division(int a, int b) throws InterruptedException {
        System.out.println("Ответ: " + (a / b));
        CustomSleep();
    }

    public static void CustomSleep() throws InterruptedException {
        Thread.sleep(2000);
    }
}
