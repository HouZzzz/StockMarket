package StockMarket.Selections;

import java.util.Scanner;

import StockMarket.Main;
import StockMarket.Stocks.StockSets;

public class Profile {
    public static Scanner scanner = new Scanner(System.in);

    // меню кошелька
    public static void ProfileMenu() throws InterruptedException {

        System.out.println("[#] Вам " + Main.years + " лет и " + Main.months + " месяцев");
        System.out.println("[#] У вас на балансе " + Main.money + " рублей\n");
        System.out.println(
                "[#] У вас " + Bank.PaidCredits + " завершенных кредитов, ваш кредитный рейтинг: " + Bank.CreditRating);

        System.out.println("[#] Мои акции");
        System.out.println("[#] Сколько вложенно в акции");
        System.out.println("[#] Цена моих акций");

        int selection = scanner.nextInt();

        SelectCases.RunProfile(selection);
    }

    // метод выводит все акции которые имеет игрок (если акций нету то
    // соответственно вывода нету)

    public static void MyStocks() throws InterruptedException {
        StockSets stockSets = new StockSets();

        if (stockSets.Sberbank.getUserGot() > 0) {
            System.out.println("Сбербанк: " + stockSets.Sberbank.getUserGot());
        }
        if (stockSets.Gasprom.getUserGot() > 0) {
            System.out.println("Газпром: " + stockSets.Gasprom.getUserGot());
        }
        if (stockSets.Lukoil.getUserGot() > 0) {
            System.out.println("Лукоил: " + stockSets.Lukoil.getUserGot());
        }
        if (stockSets.Tinkoff.getUserGot() > 0) {
            System.out.println("Тинькофф: " + stockSets.Tinkoff.getUserGot());
        }
        if (stockSets.Magnit.getUserGot() > 0) {
            System.out.println("Магнит: " + stockSets.Magnit.getUserGot());
        }
        if (stockSets.Yandex.getUserGot() > 0) {
            System.out.println("Яндекс: " + stockSets.Yandex.getUserGot());
        }
        if (stockSets.Vk.getUserGot() > 0) {
            System.out.println("ВК: " + stockSets.Vk.getUserGot());
        }
        if (stockSets.Mts.getUserGot() > 0) {
            System.out.println("МТС: " + stockSets.Mts.getUserGot());
        }
        if (stockSets.Aerofloat.getUserGot() > 0) {
            System.out.println("Аэрофлот: " + stockSets.Aerofloat.getUserGot());
        }
        if (stockSets.Ozon.getUserGot() > 0) {
            System.out.println("Озон: " + stockSets.Ozon.getUserGot());
        }
        if (stockSets.McDonalds.getUserGot() > 0) {
            System.out.println("McDonalds: " + stockSets.McDonalds.getUserGot());
        }
        if (stockSets.Nike.getUserGot() > 0) {
            System.out.println("Nike: " + stockSets.Nike.getUserGot());
        }
        if (stockSets.Disney.getUserGot() > 0) {
            System.out.println("Disney: " + stockSets.Disney.getUserGot());
        }
        if (stockSets.Mastercard.getUserGot() > 0) {
            System.out.println("Mastercard: " + stockSets.Mastercard.getUserGot());
        }
        if (stockSets.Ford.getUserGot() > 0) {
            System.out.println("Ford: " + stockSets.Ford.getUserGot());
        }
        if (stockSets.Microsoft.getUserGot() > 0) {
            System.out.println("Microsoft: " + stockSets.Microsoft.getUserGot());
        }
        if (stockSets.Apple.getUserGot() > 0) {
            System.out.println("Apple: " + stockSets.Apple.getUserGot());
        }
        if (stockSets.Tesla.getUserGot() > 0) {
            System.out.println("Tesla: " + stockSets.Tesla.getUserGot());
        }
        if (stockSets.Starbucks.getUserGot() > 0) {
            System.out.println("Starbucks: " + stockSets.Starbucks.getUserGot());
        }
        if (stockSets.Amazon.getUserGot() > 0) {
            System.out.println("Amazon: " + stockSets.Amazon.getUserGot());
        }
        if (stockSets.Intel.getUserGot() > 0) {
            System.out.println("Intel: " + stockSets.Intel.getUserGot());
        }
        if (stockSets.Netflix.getUserGot() > 0) {
            System.out.println("Netflix: " + stockSets.Netflix.getUserGot());
        }
        if (stockSets.PayPal.getUserGot() > 0) {
            System.out.println("PayPal: " + stockSets.PayPal.getUserGot());
        }
        if (stockSets.Nvidia.getUserGot() > 0) {
            System.out.println("Nvidea: " + stockSets.Nvidia.getUserGot());
        }
        if (stockSets.Ferrari.getUserGot() > 0) {
            System.out.println("Ferrari: " + stockSets.Ferrari.getUserGot());
        }
        Thread.sleep(2000);
    }

    // метод выводит сумму которую игрок потратил на акции
    public static void MoneyInStocks() throws InterruptedException {
        StockSets stockSets = new StockSets();

        if (stockSets.Sberbank.getBoughtFor() > 0) {
            System.out.println("Сбербанк: " + stockSets.Sberbank.getBoughtFor());
        }
        if (stockSets.Gasprom.getBoughtFor() > 0) {
            System.out.println("Газпром: " + stockSets.Gasprom.getBoughtFor());
        }
        if (stockSets.Lukoil.getBoughtFor() > 0) {
            System.out.println("Лукоил: " + stockSets.Lukoil.getBoughtFor());
        }
        if (stockSets.Tinkoff.getBoughtFor() > 0) {
            System.out.println("Тинькофф: " + stockSets.Tinkoff.getBoughtFor());
        }
        if (stockSets.Magnit.getBoughtFor() > 0) {
            System.out.println("Магнит: " + stockSets.Magnit.getBoughtFor());
        }
        if (stockSets.Yandex.getBoughtFor() > 0) {
            System.out.println("Яндекс: " + stockSets.Yandex.getBoughtFor());
        }
        if (stockSets.Vk.getBoughtFor() > 0) {
            System.out.println("ВК: " + stockSets.Vk.getBoughtFor());
        }
        if (stockSets.Mts.getBoughtFor() > 0) {
            System.out.println("МТС: " + stockSets.Mts.getBoughtFor());
        }
        if (stockSets.Aerofloat.getBoughtFor() > 0) {
            System.out.println("Аэрофлот: " + stockSets.Aerofloat.getBoughtFor());
        }
        if (stockSets.Ozon.getBoughtFor() > 0) {
            System.out.println("Озон: " + stockSets.Ozon.getBoughtFor());
        }
        if (stockSets.McDonalds.getBoughtFor() > 0) {
            System.out.println("McDonalds: " + stockSets.McDonalds.getBoughtFor());
        }
        if (stockSets.Nike.getBoughtFor() > 0) {
            System.out.println("Nike: " + stockSets.Nike.getBoughtFor());
        }
        if (stockSets.Disney.getBoughtFor() > 0) {
            System.out.println("Disney: " + stockSets.Disney.getBoughtFor());
        }
        if (stockSets.Mastercard.getBoughtFor() > 0) {
            System.out.println("Mastercard: " + stockSets.Mastercard.getBoughtFor());
        }
        if (stockSets.Ford.getBoughtFor() > 0) {
            System.out.println("Ford: " + stockSets.Ford.getBoughtFor());
        }
        if (stockSets.Microsoft.getBoughtFor() > 0) {
            System.out.println("Microsoft: " + stockSets.Microsoft.getBoughtFor());
        }
        if (stockSets.Apple.getBoughtFor() > 0) {
            System.out.println("Apple: " + stockSets.Apple.getBoughtFor());
        }
        if (stockSets.Tesla.getBoughtFor() > 0) {
            System.out.println("Tesla: " + stockSets.Tesla.getBoughtFor());
        }
        if (stockSets.Starbucks.getBoughtFor() > 0) {
            System.out.println("Starbucks: " + stockSets.Starbucks.getBoughtFor());
        }
        if (stockSets.Amazon.getBoughtFor() > 0) {
            System.out.println("Amazon: " + stockSets.Amazon.getBoughtFor());
        }
        if (stockSets.Intel.getBoughtFor() > 0) {
            System.out.println("Intel: " + stockSets.Intel.getBoughtFor());
        }
        if (stockSets.Netflix.getBoughtFor() > 0) {
            System.out.println("Netflix: " + stockSets.Netflix.getBoughtFor());
        }
        if (stockSets.PayPal.getBoughtFor() > 0) {
            System.out.println("PayPal: " + stockSets.PayPal.getBoughtFor());
        }
        if (stockSets.Nvidia.getBoughtFor() > 0) {
            System.out.println("Nvidea: " + stockSets.Nvidia.getBoughtFor());
        }
        if (stockSets.Ferrari.getBoughtFor() > 0) {
            System.out.println("Ferrari: " + stockSets.Ferrari.getBoughtFor());
        }
        Thread.sleep(2000);
    }

    // метод выводит сумму которую игрок получит если продаст все акции в этом
    // месяце
    public static void MyStocksPrice() throws InterruptedException {
        StockSets stockSets = new StockSets();

        if (stockSets.Sberbank.ifSellAll() > 0) {
            System.out.println("Сбербанк: " + stockSets.Sberbank.ifSellAll());
        }
        if (stockSets.Gasprom.ifSellAll() > 0) {
            System.out.println("Газпром: " + stockSets.Gasprom.ifSellAll());
        }
        if (stockSets.Lukoil.ifSellAll() > 0) {
            System.out.println("Лукоил: " + stockSets.Lukoil.ifSellAll());
        }
        if (stockSets.Tinkoff.ifSellAll() > 0) {
            System.out.println("Тинькофф: " + stockSets.Tinkoff.ifSellAll());
        }
        if (stockSets.Magnit.ifSellAll() > 0) {
            System.out.println("Магнит: " + stockSets.Magnit.ifSellAll());
        }
        if (stockSets.Yandex.ifSellAll() > 0) {
            System.out.println("Яндекс: " + stockSets.Yandex.ifSellAll());
        }
        if (stockSets.Vk.ifSellAll() > 0) {
            System.out.println("ВК: " + stockSets.Vk.ifSellAll());
        }
        if (stockSets.Mts.ifSellAll() > 0) {
            System.out.println("МТС: " + stockSets.Mts.ifSellAll());
        }
        if (stockSets.Aerofloat.ifSellAll() > 0) {
            System.out.println("Аэрофлот: " + stockSets.Aerofloat.ifSellAll());
        }
        if (stockSets.Ozon.ifSellAll() > 0) {
            System.out.println("Озон: " + stockSets.Ozon.ifSellAll());
        }
        if (stockSets.McDonalds.ifSellAll() > 0) {
            System.out.println("McDonalds: " + stockSets.McDonalds.ifSellAll());
        }
        if (stockSets.Nike.ifSellAll() > 0) {
            System.out.println("Nike: " + stockSets.Nike.ifSellAll());
        }
        if (stockSets.Disney.ifSellAll() > 0) {
            System.out.println("Disney: " + stockSets.Disney.ifSellAll());
        }
        if (stockSets.Mastercard.ifSellAll() > 0) {
            System.out.println("Mastercard: " + stockSets.Mastercard.ifSellAll());
        }
        if (stockSets.Ford.ifSellAll() > 0) {
            System.out.println("Ford: " + stockSets.Ford.ifSellAll());
        }
        if (stockSets.Microsoft.ifSellAll() > 0) {
            System.out.println("Microsoft: " + stockSets.Microsoft.ifSellAll());
        }
        if (stockSets.Apple.ifSellAll() > 0) {
            System.out.println("Apple: " + stockSets.Apple.ifSellAll());
        }
        if (stockSets.Tesla.ifSellAll() > 0) {
            System.out.println("Tesla: " + stockSets.Tesla.ifSellAll());
        }
        if (stockSets.Starbucks.ifSellAll() > 0) {
            System.out.println("Starbucks: " + stockSets.Starbucks.ifSellAll());
        }
        if (stockSets.Amazon.ifSellAll() > 0) {
            System.out.println("Amazon: " + stockSets.Amazon.ifSellAll());
        }
        if (stockSets.Intel.ifSellAll() > 0) {
            System.out.println("Intel: " + stockSets.Intel.ifSellAll());
        }
        if (stockSets.Netflix.ifSellAll() > 0) {
            System.out.println("Netflix: " + stockSets.Netflix.ifSellAll());
        }
        if (stockSets.PayPal.ifSellAll() > 0) {
            System.out.println("PayPal: " + stockSets.PayPal.ifSellAll());
        }
        if (stockSets.Nvidia.ifSellAll() > 0) {
            System.out.println("Nvidea: " + stockSets.Nvidia.ifSellAll());
        }
        if (stockSets.Ferrari.ifSellAll() > 0) {
            System.out.println("Ferrari: " + stockSets.Ferrari.ifSellAll());
        }
        Thread.sleep(2000);
    }
}
